const getDataQuran = (payload) => (dispatch, getState) => {
    dispatch({
        type: 'GET_ALL_QURAN',
        payload: payload
    })
}

const actions = {
    getDataQuran
}

export default actions