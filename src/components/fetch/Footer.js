export const Footer = () => {
    return <>
        <div className="footer bg-light justify-content-center text-center rounded-top p-2">
            <span>Made with 💗 by Fadhil</span>
        </div>
    </>
}