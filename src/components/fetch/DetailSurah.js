import React, { Component, useEffect, useState , Fragment, useCallback} from 'react';
import axios from 'axios';
import { store } from './store';
import { useParams } from 'react-router-dom';
import { Button, Card, CardContent, Container, Grid, Typography } from '@mui/material';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import DisplaySettingsIcon from '@mui/icons-material/DisplaySettings';
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';
import RepeatIcon from '@mui/icons-material/Repeat';
import ContentCopyIcon from '@mui/icons-material/ContentCopy';

const DetailSurah = () => {
    const [data, setData] = useState({});
    const [name, setName] = useState('');
    const [ayahs, setAyahs] = useState([]);
    const [user, setUser] = useState('');
    const [fs, setFs] = useState(30);
    const [lang, setLang] = useState('id');
    const [tempatTurun, setTempatTurun] = useState('')


    const { nomor } = useParams();
    useEffect(() => {
        async function fetch() {
            const response = await axios.get(`https://equran.id/api/surat/${nomor}`)
            const res = response.data
            const ayat = res.ayat
            setTempatTurun(res.tempat_turun)
            setData(res)
            setName(res.nama_latin)
            setAyahs(ayat)
        }
        fetch()
        document.body.style.background = 'darkolivegreen';
    }, [nomor])

    const btnColor = {color: 'black'}
    const textSize = {fontSize: fs, textAlign: 'right'}

    const btnClick = useCallback((e) => {
        switch(e.currentTarget.id) {
            case 'plus': if(fs !== 38) setFs(fs + 1); break;
            case 'minus': if(fs>18) setFs(fs - 1); break;
            case 'lang': setLang(lang === 'id' ? 'en' : 'id'); break;
            default: break;
        }
    }, [fs, lang])

    const copyHandler = (number, translate, ayat) => {
        navigator.clipboard.writeText(
            `${name}:${number}/${data.number} \n${ayat} \n${translate}`
            )
    }

    const handleClick = () => {
        store.dispatch({
            type: 'ADD'
        })
        console.log(store.getState().user);
        const p = document.getElementById('p')
        p.innerHTML = store.getState().user 
    }

    useEffect(() => {
        if(localStorage.getItem(`path_${nomor}`) === window.location.pathname){
            window.scrollTo(0,localStorage.getItem(`offset_${nomor}`))
        }
    })

    const handleOnMove = useCallback((e) => {
        localStorage.setItem(`path_${nomor}`, window.location.pathname)
        localStorage.setItem(`offset_${nomor}`, e.target.offsetTop)
    },[nomor])

        return (
            <div id="main-quran">
                {/* <Container className='px=0' > */}
                    <Grid container spacing={2} id='container'>
                        <Grid className="px-0" item xs={12} md={12} sx={{minWidth: 0}}>
                        <Accordion>
                                <AccordionSummary
                                expandIcon={<ExpandMoreIcon />}
                                aria-controls="panel1a-content"
                                id="panel1a-header"
                                >
                                <Typography variant='h5'>{name}</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                <Typography>
                                    Tempat turun : {tempatTurun}
                                </Typography>
                                <Typography>
                                    Jumlah ayat : {data.jumlah_ayat}
                                </Typography>
                                <Typography>
                                    Nomor surat : {data.nomor}
                                </Typography>
                                </AccordionDetails>

                            </Accordion>
                            
                        {
                            ayahs.map((value) => {
                            return <Fragment>
                            <Card  sx={{marginBottom: '20px'}}>
                            <CardContent onCopy={() => {copyHandler(value.number.inSurah, (lang === 'id') ? value.idn : value.translation.en, value.text.arab)}}>
                                <div className='row' onTouchMove={handleOnMove} >
                                    <div className='col-1 d-flex align-items-center pt-3 '>
                                        <Typography variant='subtitle1'>{value.nomor}</Typography>
                                    </div>
                                    <div className='col-10'>
                                    <Typography className="my-2 py-3" id='arab' variant="h4" sx={textSize}>{value.ar}</Typography>
                                    <Typography variant="body2" color="text.secondary">
                                        {lang === 'id' ? value.idn : value.translation.en}
                                    </Typography>
                                    </div>
                                    {window.innerWidth > 600 ?
                                    <div className='col-1 gx-0'>
                                        <Button sx={btnColor} onClick={() => copyHandler(value.nomor, (lang === 'id') ? value.idn : value.translation.en, value.text.arab)}>
                                            <ContentCopyIcon />                                        
                                        </Button>
                                    </div>: <></>}
                                </div>

                            </CardContent>
                        </Card> 
                        </Fragment> 
                            })
                        }
                        <Accordion sx={{position: 'sticky', bottom: 0 }}>
                            <AccordionSummary expandIcon={<DisplaySettingsIcon/>}>
                                Pengaturan
                            </AccordionSummary>
                            <AccordionDetails>
                                <Button sx={btnColor} id="plus" onClick={btnClick}>
                                    <AddIcon />
                                </Button>
                                <Button sx={btnColor} id="minus" onClick={btnClick}>
                                    <RemoveIcon/>
                                </Button>
                                <Button sx={btnColor} id="lang" onClick={btnClick}>
                                    <RepeatIcon/>
                                </Button>
                            </AccordionDetails>
                        </Accordion>
                        </Grid>
                    </Grid>
                {/* </Container> */}
            </div>
        )
    }

export default DetailSurah