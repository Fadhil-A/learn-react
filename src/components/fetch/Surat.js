import { Button, Card, CardContent, Typography } from "@mui/material";
import { useCallback, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

const Surat = ({isSearch, searchSurat = '', setIsLoading}) => {
    const state = useSelector(state => state);
    const [filteredData, setFilteredData] = useState([])

    const handleClickSurat = (nomor, nama_latin) => {
        localStorage.setItem('no_surat_terakhir', nomor)
        localStorage.setItem('nama_surat_terakhir', nama_latin)
    }

    useEffect(() => {
        if(searchSurat){
            const filter = state?.Quran?.data?.filter(v => (v.nomor.toString().includes(searchSurat) || v.nama_latin.toLowerCase().includes(searchSurat)))
            console.log(filter)
            setFilteredData(filter)
        }
    },[searchSurat, state?.Quran?.data])
    return <>
            <div className="row">
                {(!searchSurat && !isSearch) ?
                    state?.Quran?.data?.map((value, index) => {
                        return  <div className='col-md-4' key={index}>
                        <Card sx={{ my: 2 }} >
                        <CardContent className="text-center">
                            <div className="row py-3 border-bottom">
                                <div className="col-md-3">
                                    <Typography variant="body2">{value.nomor}</Typography>
                                </div>
                                <div className="col">
                                    <Typography variant="body2">{value.nama_latin}</Typography>
                                </div>
                            </div>
                            <Button onClick={() => handleClickSurat(value.nomor, value.nama_latin)}>
                                <Link style={{ textDecoration: 'none' }} to={`detail/${value.nomor}`}>Detail</Link>
                            </Button>
                        </CardContent>
                    </Card> 
                    </div>
                    })
                    :
                    filteredData.map((value, index) => {
                        return  <div className='col-md-4' key={index}>
                        <Card sx={{ my: 2 }} >
                        <CardContent className="text-center">
                            <div className="row py-3 border-bottom">
                                <div className="col-md-3">
                                    <Typography variant="body2">{value.nomor}</Typography>
                                </div>
                                <div className="col">
                                    <Typography variant="body2">{value.nama_latin}</Typography>
                                </div>
                            </div>
                            <Button onClick={() => handleClickSurat(value.nomor, value.nama_latin)} >
                                <Link style={{ textDecoration: 'none' }} to={`detail/${value.nomor}`}>Detail</Link>
                            </Button>
                        </CardContent>
                    </Card> 
                    </div>
                    })
                    
                }
            </div>
        </>
}

export default Surat