import axios from "axios";
import React, { useCallback, useMemo } from "react";
import { styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import { useState, useEffect } from 'react';
import { Footer } from "./Footer";
import { useDispatch, useSelector } from "react-redux";
import actions from "./actions";
import { Typography } from "@mui/material";
import Surat from "./Surat";
import { Button } from "bootstrap";
import { Link } from "react-router-dom";

const QuranFetchA = () => {

    // const [data, setData] = useState([]);
    const state = useSelector(state => state);
    const [searchSurat, setSearchSurat] = useState("");
    const [isSearch, setIsSearch] = useState(false);
    const [isLoading, setIsLoading] = useState(true);
    const lastSeen = useMemo(() => {
        return {
            nomor: localStorage?.getItem('no_surat_terakhir'),
            nama: localStorage?.getItem('nama_surat_terakhir')
        }
    },[]);

    const dispatch = useDispatch()

    const fetch = useCallback(async () => {
        await axios.get('https://equran.id/api/surat')
        .then(({data}) => {
            dispatch(actions.getDataQuran(data))
            setIsLoading(false)
        })
    },[dispatch,])

    useEffect(() => {
        fetch()
        document.body.style.background = 'darkolivegreen';
    }, [fetch, isLoading])

    const searchAyatChange = (e) => {
        setSearchSurat(e.target.value)
        if(e.target.value === ''){
            setIsSearch(false)
        }else{
            setIsSearch(true)
            setIsLoading(true)
        }
    }

    const Item = styled(Paper)(({ theme }) => ({
        ...theme.typography.body2,
        padding: theme.spacing(1),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    }));

    return (
        <div>
            <div id='main-quran'>
            <Item ><Typography variant="h6">List Surat</Typography></Item>
            <form>
                <input onChange={searchAyatChange} value={searchSurat} type="text" className="form-control" id="searchSurat" placeholder="Masukkan nomor surah / nama surah"></input>
            </form>
            {
                lastSeen.nama && (<div className='col-md-4' key={lastSeen?.nomor}>
                    <Card sx={{ my: 2 }} >
                    <CardContent className="text-center">
                        <div className="row py-3 border-bottom">
                            <div className="col-md-3">
                                <Typography variant="body2">Terakhir dibaca</Typography>
                            </div>
                            <div className="col">
                                <Typography variant="body2">{lastSeen.nama}</Typography>
                            </div>
                        </div>
                        <Link style={{ textDecoration: 'none' , marginTop: 10}} to={`detail/${lastSeen.nomor}`}>Baca</Link>
                    </CardContent>
                </Card> 
                </div>)
            }
           {(isLoading) ?  <Card>
               <CardContent>
                    <div class="d-flex justify-content-center">
                        <div class="spinner-grow text-success" role="status">
                            <span class="sr-only"></span>
                        </div>
                    </div>
               </CardContent>
           </Card> 
           : 
           <Surat
            isSearch={isSearch}
            searchSurat={searchSurat}
            setIsLoading={setIsLoading}
           />}
            {/* <Surat/> */}
            </div>
            {
                !isLoading && <Footer/>
            }
        </div>
    );
}

export default QuranFetchA
