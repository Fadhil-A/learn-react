const initialState = {
    User: {
        user: '', 
        pass: ''
    },
    Quran: {
        data: []
    }
}

export const reducer = (state = initialState, {type, payload}) => {
    switch(type){
        case 'ADD' : 
            return {
                ...state,
                User: {
                    user: 'Fadhil'
                }
            }
        case 'GET_ALL_QURAN' : 
            return {
                ...state,
                Quran: {
                    data: [
                        ...payload
                    ]
                }
            }
        default: 
    }
}