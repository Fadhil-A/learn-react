import { useEffect, useState } from "react";
import axios from "axios";

const UserFetch = () => {
    const [user, setUser] = useState(null);
    const [error, setError] = useState(false);
    const [name, setName] = useState({});
    
    useEffect(() => {
        // fetch("http://localhost:3002/user")
        // .then(response => response.json())
        // .then(data => setUser(data))
        // .catch(err => console.log(err));
        const params = JSON.stringify({ name: "Fadhil" })
        const response = axios.get("http://localhost:3002/user",params);
        if(response.status === 200) {
            const res = response.data;
            setUser(res.user);
        }else{
            console.log('error');
        }
    }, []);

    const data = {
        firstName: "",
        lastName: ""
    }

    const login = {
        email : "owner@gmail.com",
        password : "password"
    }

    const handleClick = async (e) => {
        e.preventDefault()
        console.log(e.target.value);
        const response = await axios.post('http://localhost:3002/user', data)
        const res = response.data
        console.log(res);
    }
    
    return (
        <div>
        <h1>User Fetch</h1>
            {}
            <form>
                <div className="form-group">
                    <label htmlFor="firstName">First Name</label>
                    <input type="text" className="form-control" id="firstName" aria-describedby="emailHelp" placeholder="Enter first name" />
                </div>
                <div className="form-group">
                    <label htmlFor="lastName">Last Name</label>
                    <input type="text" className="form-control" id="lastName" placeholder="last name" />
                </div>
            <button onClick={handleClick}>Input</button>
            </form>
        </div>
    );
}    

export default UserFetch;